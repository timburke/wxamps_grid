#arguments.py
#Process command line arguments

import sys
import argparse

def buildProcessor():
	parser = argparse.ArgumentParser(prog='wxamps_grid', usage="%(prog)s [options]", description='A ZeroC ICE Server for wxamps.')
	
	parser.add_argument('-e', '--path', dest='path', help="The path to the wxamps_console executable. (defaults to looking in the current directory)",
						default='wxAMPS_Console.exe')
	parser.add_argument('-p', '--port', dest='port', help="The port number to listen on for connections. (default: 10000)", 
						type=int, default=10000)
	
	return parser

def process():
	parser = buildProcessor()
	
	return parser.parse_args()