//wxgrid.ice
//Defines the interface for communicating with the wxamps_grid node

#include <protocol.ice>

module WXGrid
{
	struct Results
	{
		//Figures of Merit
		double jsc;
		double voc;
		double ff;
		double efficiency;
		
		//IV
		PC1DGrid::Protocol::DoubleArray current;
		PC1DGrid::Protocol::DoubleArray voltage;
		
		//EQE
		PC1DGrid::Protocol::DoubleArray wavelengths;
		PC1DGrid::Protocol::DoubleArray eqe;
	};
	
	struct WXGridLight
	{
		PC1DGrid::Protocol::DoubleArray wavelengths;
		PC1DGrid::Protocol::DoubleArray intensities; //W/m^2 **Absolute intensity between adjacent sampled wavelengths**
	};
	
	struct Parameters
	{
		bool 	illuminated = true;
		double 	temperature = 300.0; //K
		bool	calcQE = true;
		bool 	explicitWorkFunctions = false; //If true, use explicit work functions for top and bottom contacts, otherwise flat band assumption
		
		//Light source (required if illuminated is true)
		WXGridLight light;
		
		//Work functions in eV (ignored if explicitWorkFunctions is false)
		double phiTop;
		double phiBottom;
		
		//Reflectance (%)
		double topReflectance = 0.05;
		double bottomReflectance = 0.80;
		
		//Surface recombination currents (given in (cm/s))
		double snTop = 1e7;
		double snBottom = 1e7;
		double spTop = 1e7;
		double spBottom = 1e7;
		
		//The voltages at which the JV curve should be taken
		PC1DGrid::Protocol::DoubleArray voltages;
	};
		
	interface Node
	{
		//Solve a given device with an IV curve and EQE under the given illumination
		Results solve(PC1DGrid::Protocol::SimpleDevice device, Parameters params);
	};
};