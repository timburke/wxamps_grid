#A simple ICE server for sending parameters to and from an instance of wxamp
#running in console mode on a computer.  This is designed to work with
#the solar python modules

import sys, traceback, Ice
import os.path, os
import shutil
import csv
import subprocess

ice = Ice.initialize(sys.argv)
#Initialize all of the protocol details
Ice.loadSlice("--all -Islice/pc1d slice/wxgrid.ice")

import arguments
import WXGrid
import PC1DGrid
import threading

#Get command line argumets
args = arguments.process()

#FIXME: (Done) Add synchronization in case we have multiple callers
class NodeI(WXGrid.Node):
	def __init__(self):
		self.lock = threading.Lock()
		self.lastToken = 0
	def createSessionToken(self):
		with self.lock:
			self.lastToken += 1
			return self.lastToken
		
	def buildLightFile(self, path, params):
		f = open(path, "w")
		
		#This needs to be in (nm, W/m^2)
		for i in xrange(0, len(params.light.wavelengths)):
			line = "%.3g %.3g\n" % (	params.light.wavelengths[i], 
										params.light.intensities[i])
				
			f.write(line)
			
		#FIXME: Handle monochromatic and blackbody light
			
		
		f.close()
		
	def writeDeviceFile(self, path, device):
		f = open(path, "wb")
				
		for byte in device.deviceFile:
			f.write(chr(byte)) #need to convert to an ascii character
			
		f.close()
		
	def buildParamFile(self, path, lightPath, params):
		f = open(path, "w")
		
		lit = 1 if params.illuminated else 0
		qe = 1 if params.calcQE else 0
		explicitPhi = 1 if params.explicitWorkFunctions else 0
		
		f.write("%d\n" % lit)
		f.write("%s\n" % lightPath)
		f.write("%.0g\n" % params.temperature)
		f.write("%d\n" % qe)
		f.write("%d\n" % explicitPhi)
		
		if explicitPhi == 1:
			f.write("%.2g %.2g " % (params.phiTop, params.phiBottom))
		
		f.write("%.2g %.2g %.2g %.2g %.2g %.2g\n" % 
				(params.snTop, params.spTop, params.topReflectance,
				 params.snBottom, params.spBottom, params.bottomReflectance))
		
		voltageString = " ".join( map(lambda x: ("%.2g" % x), params.voltages) )
		
		f.write("%s\r\n" % voltageString)
		
		f.close()
		
		return [path, lightPath]
	
	def removeFiles(self, paths):
		for path in paths:
			if os.path.exists(path):
				os.remove(path)
		
	#wxAMPS_Console puts the results in a directory with a special name
	def buildResultsDir(self, paramFile, devFile):
		name = paramFile + "_" + devFile

		#remove possible directory specifiers
		name = name.replace(":", "_")
		name = name.replace("\\", "_")
		
		return name
		
	def readIV(self, paramFile, devFile):
		path = os.path.join(self.buildResultsDir(paramFile, devFile), "IV.csv")
		
		file = open(path, "r")
		
		#first line is headers
		file.readline()
		meritLine = file.readline()
		file.readline() #if the device was lit
		file.readline()	#number of iv points
		file.readline() #column names
		
		#now we're reading the IV data
		voltage = []
		current = []
		
		while True:
			line = file.readline()
			line.rstrip() #chomp postpended whitespace
			
			#Check if we're at the end of the file
			if len(line) == 0:
				break
				
			(voltageS,currentS) = line.split(',')
			voltage.append(float(voltageS))
			current.append(float(currentS))
		
		#now parse the figures of merit
		meritLine.rstrip()
		(voc, jsc, ff, eff) = meritLine.split(',')
		
		results = {}
		results["Voltage"] = voltage
		results["Current"] = current
		results["Voc"] = float(voc)
		results["Jsc"] = float(jsc)
		results["FF"] = float(ff)
		results["Efficiency"] = float(eff)
		
		file.close()
		
		return results
	
	def readQE(self, paramFile, devFile):
		path = os.path.join(self.buildResultsDir(paramFile, devFile), "QE.csv")
		
		file = open(path, "r")
		
		wavelength = []
		qe = []
		
		#skip header row
		file.readline()
		
		while True:
			line = file.readline()
			line.rstrip() #chomp postpended whitespace
			
			#Check if we're at the end of the file
			if len(line) == 0:
				break
			
			(waveS, qeS) = line.split(",")
			
			wavelength.append(float(waveS))
			qe.append(float(qeS))
		
		results = {}
		results["Wavelengths"] = wavelength
		results["EQE"] = qe
		
		file.close()
		
		return results
		
	def removeResults(self, paramFile, devFile):
		resultsDir = self.buildResultsDir(paramFile, devFile)
		
		shutil.rmtree(resultsDir, ignore_errors=True)
		
	
	def buildPath(self, name, token):
		return "session-%s-%d" % (name,token)
	
	#Ice entry point for solving a device setup
	def solve(self, device, params, ctx):
		global args
		token = self.createSessionToken() #get a unique token for this session
		
		lightPath = self.buildPath("light", token)
		devicePath = self.buildPath("device", token)
		paramPath = self.buildPath("param", token)
		
		paths = (lightPath, devicePath, paramPath)
		
		#make sure the files don't exist
		self.removeFiles(paths)
		
		#create the control files for wxamps_Console
		self.buildLightFile(lightPath, params)		
		self.buildParamFile(paramPath, lightPath, params)
		self.writeDeviceFile(devicePath, device)
	
		#actually call the solver
		subprocess.call([args.path, paramPath, devicePath])
		
		#FIXME: Check for convergence or other issues with the call to the prog
		#get the results and merge them
		data = self.readIV(paramPath, devicePath)
		
		if params.calcQE:
			data.update(self.readQE(paramPath, devicePath))
		
		#Get rid of the temporary files
		self.removeFiles(paths)
		self.removeResults(paramPath, devicePath)
		
	
		results = WXGrid.Results()
		
		results.jsc = data["Jsc"]
		results.voc = data["Voc"]
		results.ff = data["FF"]
		results.efficiency = data["Efficiency"]
		results.current = data["Current"]
		results.voltage = data["Voltage"]
		
		#If we calculated a QE, fill it in
		if params.calcQE:
			results.wavelengths = data["Wavelengths"]
			results.eqe = data["EQE"]
		
		return results

#Write out some status information
print "\n************"
print "Starting WxAmps Grid Node"
print "Listening on port: %d" % args.port
print "Configured with wxamps executable path:", args.path
print "************\n"
status = 0
ic = None
try:
    ic = Ice.initialize(sys.argv)
    adapter = ic.createObjectAdapterWithEndpoints("WXGridAdapter", "default -p %d" % args.port)
    object = NodeI()
    adapter.add(object, ic.stringToIdentity("WXAmps"))
    adapter.activate()
    ic.waitForShutdown()
except:
    traceback.print_exc()
    status = 1

if ic:
    # Clean up
    try:
        ic.destroy()
    except:
        traceback.print_exc()
        status = 1

sys.exit(status)