#setup.py
#Config file for using py2exe to make a portable executable

from distutils.core import setup
import py2exe

files = ["ice34.dll", "iceutil34.dll", "bzip2.dll", "msvcr90.dll", "slice34.dll", "Solve.dll", "Solve.lib", "wxAMPS_Console.exe", "README"]

setup(	console=['wxamps_grid.py'], 
		data_files=files)